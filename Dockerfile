FROM alpine:latest

# Update
RUN apk update

# Install dependencies
RUN apk add nodejs npm

# Create an unpriviledged user for the app
RUN adduser -D -H discordbot

# Copy the source
ADD ./src /discordbot
WORKDIR /discordbot

# Download dependencies
RUN npm install

# Expose sys (used for gpio)
VOLUME ["/sys"]

# Setup environment
RUN chown -R discordbot:discordbot /discordbot
USER discordbot

# Start the bot
ENTRYPOINT [ "npm run app" ]
