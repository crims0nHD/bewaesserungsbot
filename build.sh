#!/usr/bin/env bash

IMAGE_NAME="makerspace/bewaesserungsbot"

echo "Building $IMAGE_NAME"
docker build -t $IMAGE_NAME .
echo "Done building $IMAGE_NAME"
