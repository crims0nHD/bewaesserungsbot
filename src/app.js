console.log("Starting BewässerungsBot!");

// standard modules
const process = require("process");
const fs = require("fs");
const path = require("path");

// discord.js
const { Client, Events, GatewayIntentBits } = require("discord.js");

// get tokens from env variables
const token = process.env["DISCORD_BOT_TOKEN"];
console.log("Successfully read bot token!");
const clientid = process.env["DISCORD_BOT_CLIENT_ID"];
console.log("Successfully read application's client id!");

const client = new Client({ intents: [GatewayIntentBits.Guilds] });

// Commands
// ---------
// Add every command in the "./commands" directory to the list
client.commands = new Map();

// Get all files in "commands" directory
const commandsPath = path.join(__dirname, "commands");
const commandsFiles = fs.readdirSync(commandsPath).filter(f => f.endsWith(".js"));

for (const fName of commandsFiles){
    // Get path to the file
    const fPath = path.join(commandsPath, fName);
    // Import the command into "const command"
    const command = require(fPath);

    // Check if the command module is made with the command builder class
    if("data" in command && "execute" in command){
        client.commands.set(command.data.name, command);
        console.log("Successfully added command " + command.data.name);
    }
    else
    {
        console.log("[WARNING] The command at " + fPath + " is missing a required \"data\" or \"execute\" property.");
    }
}

// Events
// ---------
// On Login
client.once(Events.ClientReady, c => {
    console.log("Bot started and logged in as " + c.user.tag);
})

// On Interaction
client.on(Events.InteractionCreate, async interaction => {
    // If the interaction is not a "/" command then do nothing
    if (!interaction.isChatInputCommand()) return;

    // Try and get the command from the collection
    const command = interaction.client.commands.get(interaction.commandName);

    if (!command){
        console.error("Command " + interaciton.commandName + " was not found!");
        return;
    }

    try{
        await command.execute(interaction);

    } catch (error){
        console.error(error);
        await interaction.reply({content: "There was an error!", ephemeral: true});
    }
});

// Login
client.login(token);
