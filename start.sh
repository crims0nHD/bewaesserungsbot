#!/usr/bin/env bash

set quit=0

if [[ -z $DISCORD_BOT_TOKEN ]] then
   echo "Environment variable DISCORD_BOT_TOKEN not defined!"
   quit=1
fi
if [[ -z $DISCORD_BOT_CLIENT_ID ]] then
   echo "Environment variable DISCORD_BOT_CLIENT_ID not defined!"
   quit=1
fi

if [[ quit -eq 1 ]] then
    echo "Exiting..."
    exit 1
fi

IMAGE_NAME="makerspace/bewaesserungsbot"
CONTAINER_NAME="bewaesserungsbot"

echo "Starting the container \"$CONTAINER_NAME\" with the image \"$IMAGE_NAME\""
docker run --name $CONTAINER_NAME \
    -v /sys:/sys \
    -e DISCORD_BOT_TOKEN \
    -e DISCORD_BOT_CLIENT_ID \
    $IMAGE_NAME -d
echo "Started the container \"$CONTAINER_NAME\"... Check the status with \"docker ps\"!"
